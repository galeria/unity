package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.services.banners.IUnityBannerListener;
import com.unity3d.services.banners.UnityBanners;

public class MainActivity extends AppCompatActivity {

    private String GameID = "3974643";
    private boolean testMode = true;
    private String bannerAdPlacement = "banner";
    private String interstitialAdPlacement = "interstitial";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UnityAds.initialize (MainActivity.this, GameID, testMode);

        IUnityBannerListener iUnityBannerListener = new IUnityBannerListener() {
            @Override
            public void onUnityBannerLoaded(String s, View view) {
                ((ViewGroup) findViewById (R.id.bannerAdLayout)).removeView (view);
                ((ViewGroup) findViewById (R.id.bannerAdLayout)).addView (view);

            }

            @Override
            public void onUnityBannerUnloaded(String s) {

            }

            @Override
            public void onUnityBannerShow(String s) {

            }

            @Override
            public void onUnityBannerClick(String s) {

            }

            @Override
            public void onUnityBannerHide(String s) {

            }

            @Override
            public void onUnityBannerError(String s) {

            }
        };

        UnityBanners.setBannerListener(iUnityBannerListener);
        UnityBanners.loadBanner(MainActivity.this, bannerAdPlacement);


        IUnityAdsListener unityAdsListener = new IUnityAdsListener() {
            @Override
            public void onUnityAdsReady(String s) {
                Toast.makeText(MainActivity.this, "onUnityAdsReady", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUnityAdsStart(String s) {
                Toast.makeText(MainActivity.this, "onUnityAdsStart", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                Toast.makeText(MainActivity.this, "onUnityAdsFinish", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                Toast.makeText(MainActivity.this, "onUnityAdsError", Toast.LENGTH_LONG).show();
            }
        };

        UnityAds.setListener(unityAdsListener);
        UnityAds.load(interstitialAdPlacement);
        DisplayInterstitialAd();



    }

    private void DisplayInterstitialAd() {

        if(UnityAds.isReady(interstitialAdPlacement)) {
            UnityAds.show(MainActivity.this, interstitialAdPlacement);
        }

    }
}